# Real Time Object Detection with Alert
Predicts objects coming across mobile camera using Clarifai General Model API

There is an android app called IP Webcam that has been installed on mobile phone. We have to start the server of IP Webcam which which capture the things coming across the camera and sent it to the application.
We need Clarifai Account to use the API for object Detection. The script is written to detect the objects coming across mobile camera using Django Web Framework. 
Frontend of the application is as shown below:

<p align="center">
  <img src="frontend_screenshot.png" width=900 height=550>
</p>

When a person comes in front of the camera, the system will generate alert by sending a text message to the number specified in python script. The screenshot of text message is as shown below:

<p align="center">
  <img src="mobile_screenshot.png" width=250 height=450>
</p>

