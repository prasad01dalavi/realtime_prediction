# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Third Party App Imports
from rest_framework.views import APIView
from rest_framework.response import Response

from clarifai.rest import ClarifaiApp
from clarifai.rest import Image as ClImage
from StringIO import *
import requests

app = ClarifaiApp()
model = app.models.get('general-v1.3')

import way2sms                  # Importing module to send the alert text messages
message = way2sms.sms('8983050329', 'betheone')  # My way2sms login credentials
mobile_number = '8983050329'        # Mobile number to which the alert message will be sent


class PredictImageObject(APIView):
    def alert(self):        # This will send the alert message to the number specified
        try:
            message = way2sms.sms('8983050329', 'betheone')
            message.send(mobile_number, 'Warning!!! There is a person outside.')
            sent_count = message.msgSentToday()
            print 'Message Sent Count =', sent_count
            message.logout()
        except:
            print 'Message sending Failed!'

    def post(self, request):    # this is a post request to post the alert button status and will be responsed with the calculated parameters
        analysis = []
        resp = requests.get('http://192.168.0.100:8080/shot.jpg')
        imgbytes = resp.content
        image = ClImage(file_obj=StringIO(imgbytes))
        response = model.predict([image])
        concepts = response['outputs'][0]['data']['concepts']
        for concept in concepts:
            if concept['name'] == 'people' and request.data['alert'] == 'true' and concept['value'] > 0.95:   # send alert message
                self.alert()
            analysis.append({
                'label': concept['name'],
                'confidence': str(concept['value'] * 100)[:5]
            })                  # Make a dictionary(json object) to display the parameters on the screen
        return Response(analysis)


def PageLoad(request):  # Loads the Object detection site
    if request.method == 'GET':
        return render(request, "video_response.html")
